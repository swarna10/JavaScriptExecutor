/**
 * 
 */
var myObj =  
[{'name':'Arun', 'age': 30, 'occupation': "Developer"},  
{'name':'Ashish', 'age': 32, 'occupation': "Developer"},  
{'name':'Kalyani', 'age': 25, 'occupation': "Programmer"},  
{'name':'David', 'age': 27, 'occupation': "Programmer"},  
{'name':'Priya', 'age': 22, 'occupation': "Programmer"},  
{'name':'Venkat', 'age': 28, 'occupation': "Programmer"}];
function sortObjectArray(){
	
	var library = [   
		  { film: 'Lagaan', rating: '5', year: 2003},  
		  { film: 'Bahubali', rating: '4', year: 2015},  
		  { film: 'Godfather', rating: '4', year: 1990}  
		]; 
	
	library.sort(function(a, b) {
		return a.year-b.year;
	})
	console.log("sorted object array:"+JSON.stringify(library))
}


function filterArray(){
	var newArray = myObj.filter(function (el) {
		  return el.occupation ="Programmer"
		});
	console.log("filtered array:"+JSON.stringify(newArray))
	
}

function createNewObject(){
	var obj=myObj;
	var mayArray=new Array();
	for(var i=0;i<obj.length;i++){
		mayArray[i]=obj[i]['occupation'];
	}
	
	mayArray = mayArray.filter(function(item, pos) {
	    return mayArray.indexOf(item) == pos;
	})
	
	var newObj=new Object();
	for(var i=0;i<mayArray.length;i++)
	{
		newObj[mayArray[i]]=new Array();
	}
	for(var i=0;i<obj.length;i++){
		var occupation=obj[i]['occupation'];
		newObj[occupation][i]=obj[i];
	}
	
	console.log("new array:"+JSON.stringify(newObj))
	
}
/*sortObjectArray();
filterArray();*/
createNewObject();
