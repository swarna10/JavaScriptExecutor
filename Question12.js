/**
 * 
 */
function sortString(a){
	
	var b=a.split('').sort(caseInsensitiveSort).join('');
	function caseInsensitiveSort(a, b)
	{
	   var ret = 0;
	   a = a.toLowerCase();b = b.toLowerCase();
	   if(a > b)
	      ret = 1;
	   if(a < b)
	      ret = -1;
	   return ret;
	}
	return b;
}

console.log(sortString("webmaster"));
