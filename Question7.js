function sortObjectArray(){
	var library = [   
		  { film: 'Lagaan', rating: '5', year: 2003},  
		  { film: 'Bahubali', rating: '4', year: 2015},  
		  { film: 'Godfather', rating: '4', year: 1990}  
		]; 
	
	library.sort(function(a, b) {
		return a.year-b.year;
	})
	console.log("sorted object array:"+JSON.stringify(library))
}
sortObjectArray();
